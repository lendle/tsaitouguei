import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import rocks.imsofa.tsaitouguei.*;

void doGet(TTGContext context, HttpServletRequest request, HttpServletResponse response) throws Exception{
        System.out.println(new Gson().toJson(context.getVars()));
        System.out.println(new Gson().toJson(context.getParams()));
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out=new PrintWriter(response.getWriter());
        out.println("Hello World!");
        out.flush();
        out.close();
}
