/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.tsaitouguei;

import java.io.File;
import java.io.IOException;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 *
 * @author M4630G_win7
 */
public class TTGServer {

    private int port = -1;
    private Dispatcher dispatcher = new Dispatcher();
    private Server server = null;

    public TTGServer(int port, String resourceBase) {
        this.port = port;
        server = new Server(port);
        ServletContextHandler context = new ServletContextHandler(
                ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.setResourceBase(resourceBase);
        server.setHandler(context);
        context.addServlet(DefaultServlet.class, "/");
        context.addServlet(new ServletHolder(dispatcher), "/services/*"); 
    }
    
    public TTGServer(int port) throws IOException{
        this(port, new File(".").getCanonicalPath());
    }

    public TTGServer() throws IOException{
        this(9000);
    }
    
    public int getPort() {
        return port;
    }

    public void addHandler(String pathSpec, Object handler) {
        dispatcher.addHandler(pathSpec, handler);
    }

    public void start() throws Exception {
        server.start();
    }

    public void join() throws InterruptedException {
        server.join();
    }

    public void destroy() {
        server.destroy();
    }
}
