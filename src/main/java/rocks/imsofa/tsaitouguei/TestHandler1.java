/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.tsaitouguei;

import com.google.gson.Gson;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lendle
 */
public class TestHandler1 {
    public void doGet(TTGContext context, HttpServletRequest request, HttpServletResponse response) throws Exception{
        System.out.println(new Gson().toJson(context.getVars()));
        System.out.println(new Gson().toJson(context.getParams()));
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out=new PrintWriter(response.getWriter());
        out.println("Hello World!");
        out.close();
    }
}
