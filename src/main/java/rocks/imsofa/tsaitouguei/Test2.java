/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.tsaitouguei;

import org.codehaus.groovy.tools.GroovyStarter;

/**
 *
 * @author lendle
 */
public class Test2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        System.setProperty("groovy.home", "/home/lendle/dev/NetBeansProjects/LabUtils/tsaitouguei/deploy/ttg");
        GroovyStarter.rootLoader(new String[]{
            "--classpath", "/home/lendle/dev/NetBeansProjects/LabUtils/tsaitouguei/deploy",
            "--main", "groovy.ui.GroovyMain",
            "--conf", "/home/lendle/dev/NetBeansProjects/LabUtils/tsaitouguei/deploy/ttg/conf/groovy-starter.conf",
            "/home/lendle/dev/NetBeansProjects/LabUtils/tsaitouguei/deploy/test.groovy"
        });
    }
    
}
