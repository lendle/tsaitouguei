/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.tsaitouguei;

import com.sun.jersey.api.uri.UriTemplate;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

/**
 *
 * @author M4630G_win7
 */
public class Dispatcher extends HttpServlet {

    private Map<String, Object> handlers = new HashMap<>();

    public void addHandler(String pathSpec, Object handler) {
        handlers.put(pathSpec, handler);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResolveResult result = this.resolve(req);
        try {
            Method method = result.handler.getClass().getMethod("doTrace", TTGContext.class, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(result.handler, result.context, req, resp);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResolveResult result = this.resolve(req);
        try {
            Method method = result.handler.getClass().getMethod("doOptions", TTGContext.class, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(result.handler, result.context, req, resp);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResolveResult result = this.resolve(req);
        try {
            Method method = result.handler.getClass().getMethod("doDelete", TTGContext.class, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(result.handler, result.context, req, resp);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResolveResult result = this.resolve(req);
        try {
            Method method = result.handler.getClass().getMethod("doPut", TTGContext.class, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(result.handler, result.context, req, resp);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResolveResult result = this.resolve(req);
        try {
            Method method = result.handler.getClass().getMethod("doPost", TTGContext.class, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(result.handler, result.context, req, resp);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResolveResult result = this.resolve(req);
        try {
            Method method = result.handler.getClass().getMethod("doHead", TTGContext.class, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(result.handler, result.context, req, resp);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResolveResult result = this.resolve(req);
        try {
            Method method = result.handler.getClass().getMethod("doGet", TTGContext.class, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(result.handler, result.context, req, resp);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    private ResolveResult resolve(HttpServletRequest req) throws ServletException {
        TTGContext context = new TTGContext();
        String uri = req.getPathInfo();
        for (String path : this.handlers.keySet()) {
            UriTemplate uriTemplate = new UriTemplate(path);
            Map<String, String> vars = new HashMap<>();
            if (uriTemplate.match(uri, vars)) {
                context.setVars(vars);
                context.setServlet(this);
                Enumeration<String> parameterNames = req.getParameterNames();
                Map<String, String> params = new HashMap<>();
                while (parameterNames.hasMoreElements()) {
                    String parameterName = parameterNames.nextElement();
                    params.put(parameterName, req.getParameter(parameterName));
                }
                context.setParams(params);
                //process files
                Map<String, FileItemStream> files = new HashMap<>();
                context.setFiles(files);
                boolean isMultipart = ServletFileUpload.isMultipartContent(req);
                if (isMultipart) {
                    try {
                        ServletFileUpload upload = new ServletFileUpload();
                        FileItemIterator iter = upload.getItemIterator(req);
                        while (iter.hasNext()) {
                            FileItemStream item = iter.next();
                            String name = item.getFieldName();
                            if (item.isFormField()) {
                                params.put(item.getFieldName(), Streams.asString(item.openStream(), "utf-8"));
                            } else {
                                files.put(item.getFieldName(), item);
                            }
                        }
                    } catch (Exception ex) {
                        throw new ServletException(ex);
                    }
                }
                ///////////////

                ResolveResult result = new ResolveResult();
                result.context = context;
                result.handler = this.handlers.get(path);
                return result;
            }
        }
        throw new ServletException("no matched handler");
    }

    class ResolveResult {

        TTGContext context = null;
        Object handler = null;
    }
}
