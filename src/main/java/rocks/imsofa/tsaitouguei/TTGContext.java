/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.tsaitouguei;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.Servlet;
import org.apache.commons.fileupload.FileItemStream;

/**
 *
 * @author lendle
 */
public class TTGContext {
    private Servlet servlet=null;
    private Map<String, String> params=new HashMap<>();
    private Map<String, String> vars=new HashMap<>();
    private Map<String, FileItemStream> files=new HashMap<>();

    public Servlet getServlet() {
        return servlet;
    }

    public void setServlet(Servlet servlet) {
        this.servlet = servlet;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Map<String, String> getVars() {
        return vars;
    }

    public void setVars(Map<String, String> vars) {
        this.vars = vars;
    }

    public Map<String, FileItemStream> getFiles() {
        return files;
    }

    public void setFiles(Map<String, FileItemStream> files) {
        this.files = files;
    }
    
    
}
