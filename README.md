# TsaiTouGuei

**Tsai Tou Guei** (the pronounciation of carrotcake in Taiwanese) is a simple and lightweight
Java server framework (based on the servlet spec) for fast prototyping of web sites and web services and 
can be used in production in some simple cases.
Typically, to create a web site or a web service with Java technologies requires the deployment of
a Java servlet container (e.g. Tomcat, Jetty, etc.) and the construction of a Java web application.
With the help of modern IDEs, this is usually not considered as a difficult task. However, compared with
other server-side frameworks such as nodejs and python, the procedure of building a Java based web site or
web service is still overly tedious, especially if one is just prototyping an idea. For example,
to build a web service in python, one can simply use *Flask* as a framework and server combination:

	FLASK_APP=hello.py flask run

In such a way, one can focus on the business logic without putting too much attention on the server-deployment
procedure. **Tsai Tou Guei** is thus developed.


Benefiting from the embedded **groovy** technology, **Tsai Tou Guei** is script-driven. The installation is
fairly easy. After extracting the zipped archive file, users should add the *bin* folder into their *PATH*
environment variable. The main executable is *ttg* for linux-like and *ttg.bat* for windows-like operating
systems. *Groovy* supports almost all Java syntax, so does **Tsai Tou Guei**. To create a web service, the simplest
way is to create two script files. The first file lauches the server and register handlers while the second
file implements the service logic. For instance, in the example below, *test.groovy* is the launcher while *TestHandler1.groovy*
implements a web service:


test.groovy:
```
	import rocks.imsofa.tsaitouguei.*;
	TTGServer server=new TTGServer();
	server.addHandler("/handler1/{var1}/{var2}", new TestHandler1());
	try{
		server.start();
		server.join();
	}finally{
		server.destroy();
	}
```	
	
TestHandler1.groovy:
```
	import com.google.gson.Gson;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;
	import java.io.*;

	void doGet(TTGContext context, HttpServletRequest request, HttpServletResponse response) throws Exception{
			System.out.println(new Gson().toJson(context.getVars()));
			System.out.println(new Gson().toJson(context.getParams()));
			response.setContentType("text/html;charset=utf-8");
			PrintWriter out=new PrintWriter(response.getWriter());
			out.println("Hello World!");
			out.close();
	}
```
